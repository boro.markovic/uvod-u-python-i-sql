from email import charset
from multiprocessing import connection
from unittest import result
import pymysql.cursors
connection=pymysql.connect(host='localhost',
user='boromarkovic',
password='root',
database='proba',
charset='utf8mb4',
cursorclass=pymysql.cursors.DictCursor)

##UNOS PODATAKA ZA FIRMA ZAPOSLENI
#with connection:
    #with connection.cursor() as cursor:
        #sql="INSERT INTO `firmazaposleni`(`id`,`od`,`ime`,`telefon`,`datum`) values (%s,%s,%s,%s,%s)"
        #cursor.execute(sql,('14','50','Djordje Petrovic','111222','2017-02-02'))
    #connection.commit()
#ID mora biti uvijek drugaciji 



##UNOS PODATAKA ZA FIRMA ODJELJENJE
#with connection:
    #with connection.cursor() as cursor:
        #sql="INSERT INTO `firmaodjeljenje`(`idod`,`naziv`,`lokacija`,`rukovidlac`) values (%s,%s,%s,%s)"
        ##cursor.execute(sql,('50','Parking','Podgorica','Djordje Petrovic'))
    #connection.commit()


##SELECT ZA IMENA IZ TABELE FIRMAZAPOSLENI
with connection.cursor() as cursor:
    sql=""" select ime from firmazaposleni;"""
    cursor.execute(sql)
    result= cursor.fetchall()
    print(f"{result}\n")
        
#SELECT za sve brojeve telefona koji rade u niksicu 
with connection.cursor() as cursor:
    sql="""select telefon,ime,od from firmazaposleni x,firmaodjeljenja y where x.od=y.idod and y.lokacija='Niksic';"""
    cursor.execute(sql)
    result=cursor.fetchall()
    print(f"\n{result}\n")
#SELECT za zaposlenog sa najvecim radnim stazom i imenom Marko Markovic
with connection.cursor() as cursor:
    sql=""" select min(y.datum) datum,y.ime from proba.firmaodjeljenja x  left join proba.firmazaposleni y  on y.od=x.idod where ime='Marko Markovic' ;"""
    cursor.execute(sql)
    result=cursor.fetchone()
    print(f"Rezultat je :\n {result}")



#Delete zaposlene koji rade u marketingu
#with connection.cursor() as cursor:
    #sql="""delete from firmazaposleni where od=10; """
    #cursor.execute(sql)
#connection.commit()

with connection.cursor() as cursor:
    sql="""select od from firmazaposleni;"""
    cursor.execute(sql)
    result=cursor.fetchall()
    print(f"\n{result}\n")