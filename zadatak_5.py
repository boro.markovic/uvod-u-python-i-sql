from pydoc import plainpager
from zadatak_4 import Osoba, najstarija
class Radnik(Osoba):
    def __init__(self, ime, prezime, JMBG, opstina, datum,zanimanje,plata,god_staza):
        super().__init__(ime, prezime, JMBG, opstina, datum)
        self.zanimanje=zanimanje
        self.__plata=plata
        self.god_staza=god_staza
    def get_zanimanje(self):
        return self.zanimanje
    
    def get_plata(self):
        return self.__plata
    
    def get_god_staza(self):
        return self.god_staza
    
    def stampaj(self):
        osoba=f"""
        Ime:{self.ime},
        Prezime:{self.prezime},
        JMBG:{self.JMBG},
        Opstina:{self.opstina},
        Datum rodjenja:{self.datum}
        Zanimanje:{self.zanimanje}
        Plata:{self.__plata}
        Godine Staza:{self.god_staza}
        """
        print(osoba)
    def god_prihod(self):
        prihod=12*self.__plata
        return (f"{self.ime} {self.prezime}", f"{self.datum}", self.god_staza, prihod)
        #return '{} {} {} {} {}'.format(self.ime, self.prezime, self.datum, self.god_staza, prihod)

#izgasio sam input metodu u zadatku_4 i makao pozivanje za stampanje
radnici=[Radnik("Boro","Markovic","1234567890123","Podgorica","1999-30-12","vozac",500,3),
Radnik("Miso","Misic","2987654321098","Andrijevica","1976-09-11","Sef",1000,12),
Radnik("Petar","Cetkovic","3987654321098","Beranec","1976-08-11","Magacioner",950,10),
Radnik("Ivan","Ivanovic","4987654321098","Cetinje","1976-07-11","Pomocni sef",900,8),
Radnik("Budo","Tomovic","5987654321098","Mojkovac","1976-06-11","Sef magacina",980,11)]

def datum(radnici):
    return sorted(radnici, key=lambda radnik: radnik.get_datum() )
    
def staz(radnici):
    return sorted(radnici, key=lambda radnik : radnik.get_god_staza())

def godprihod(radnici):
    return sorted(radnici,key=lambda radnik: radnik.god_prihod()[3],reverse=True)



gdstaz=staz(radnici)
god=datum(radnici)
print("Radnici sortirani po godisnjem prihodu od najveceg ka najmanjem")
prihod=godprihod(radnici)
for radnik in prihod:
    radnik.stampaj()

if gdstaz==god:
    print("Ove dvije liste su iste")
else:
    print("Ove dvije liste nisu iste")