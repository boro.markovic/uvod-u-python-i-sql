from cProfile import label
import matplotlib.pyplot as plt
from numpy import positive
from zadatak_3_funkcija import csv_get

positive,hospitalized = csv_get("covid_februar.csv")

days=list(range(1,29)) #dani za mjesec februar
plt.plot(days,positive,label='pozitivni')
plt.plot(days,hospitalized,label='hospitalizovanih')
plt.suptitle('Broj pozitivnih i zarazenih')
plt.xlabel('Broj dana')
plt.ylabel('broj pozitivnih i hospitalizovanih')
plt.legend(['pozitivni','hospitalizovani'])


plt.show()





