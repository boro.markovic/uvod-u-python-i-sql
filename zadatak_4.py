import datetime
from gettext import find
import re


class Osoba:
    def __init__(self,ime,prezime,JMBG,opstina,datum):
        self.ime=ime
        self.prezime=prezime
        self.JMBG=JMBG
        self.opstina=opstina
        self.datum=datum

    #getters 
    def get_ime(self):
        return self.ime  

    def get_prezime(self):
        return self.prezime  
    
    def get_JMBG(self):
        return self.JMBG
    
    def get_opstina(self):
        return self.opstina
    
    def get_datum(self):
        return self.datum
    
    
#setters
    
    def set_ime(self,ime):
        self.ime=ime
    
    def set_prezime(self,prezime):
        self.prezime=prezime
    
    def set_JMBG(self, JMBG):
        if len(JMBG)==13:
            self.JMBG=JMBG
        else:
            raise TypeError("JMBG mora imati 13 cifara")

    
    def set_opstina(self,opstina):
        self.opstina=opstina
    
    def set_datum(self,datum):
        obrazac="\d{4}-\d{2}-\d{2}"
        if not(re.search(obrazac,datum)):
            raise TypeError("Datum nije u fromatu god-mj-dan")
        date=datetime.datetime.strptime(datum,'%Y-%m-%d')
        danasnji=datetime.datetime.now()
        if danasnji>date:
            self.datum=datum
        else:
           raise TypeError("Ovaj datum nije validan") 
    def stampaj(self):
        osoba=f"""
        Ime:{self.ime},
        Prezime:{self.prezime},
        JMBG:{self.JMBG},
        Opstina:{self.opstina},
        Datum rodjenja:{self.datum}
        """
        print(osoba)

    def __lt__(self, other):
        return self.datum < other.datum
        

osobe=[]
n=int(input("Broj osoba koji zelite da unesete: "))
for i in range(n):
    info=input("Unesite: ime,prezime,JMBG,opstina,datum ")
    info=info.split(',')
    osobe.append(Osoba(info[0],info[1],info[2],info[3],info[4]))
    print()


def najstarija(osobe):
    osobe = sorted(osobe, key = lambda x : x.datum)
    return osobe[0]
a=osobe[0].datum
b=osobe[1].datum
print(a>b)

x=najstarija(osobe)
x.stampaj()
