from ast import Dict
from cgi import print_form
from curses import doupdate, keyname
from nis import maps
from operator import itemgetter



developers ={ 'Marshal': ['Company website', 'Animal recognition', 'Fashion store website'], 
'Ted': ['Plants watering system', 'WHAT', 'Animal recognition', 'Food recognition'], 
'Monica': ['NEXT website', 'Fashion store website'], 
'Phoebe': ['WHAT', 'Company website', 'NEXT website'],}

project0=[]
for developer in developers:
    project0 +=developers[developer]
print(f"\nnova lista project je {project0}\n")

#koristimo set jer se moze mijenjati za razliku od tuple-a,i ne moze da se duplira
project_set=set(project0)
print(f'nova lista je {project_set} \n')
#dodavanje developera i liste projekata
project1={}
for project in project_set:
    project1[project]= []
    for developer in developers:
        if project in developers[developer]:
            project1[project].append(developer)

print(f'projekat je \n{project1}')
#dodavanje BIXBIt ispred imena projekta
project0=list(map(lambda project:"BIXBIT_" +project,project0))
print(f'Nova lista je \n{project0}\n')
def test(developer,list_of_developers):
    developers[developer] = list_of_developers
    for project in list_of_developers:
        if project not in project1:
            project1[project]=[]
        project1[project].append(developer)
    return len(developers)
broj_developera=test('Boro',['NEXT website','WHAT','Animal recognition'])


print(broj_developera)
print(f'azurirani dictionary je :\n{developers}')

#dodavanje sati i zarade
def dodaj(dodavanje,vrijeme,zarada,):
    for developer in developers:
        if developer in project1[dodavanje]:
            developers[developer]=[(project, vrijeme,zarada) if project==dodavanje 
            else project for project in developers[developer]]

dodaj('Company website', '180h', 3200)
dodaj('NEXT website','300h', 4000)
dodaj('WHAT', '50h',1000)
dodaj('Animal recognition','400h',6000)
dodaj('Food recognition','70h',2000)
dodaj('Fashion store website','200',3000)
dodaj('Plants watering system','45h',3400)

print(developers)
#prikazivanje koliko ko radi i zaradjuje 
def proba():
    poruka = ""
    for developer in developers:
        total_profit, total_hours = 0, 0
        for devs_project in developers[developer]:
            profit = devs_project[2]
            
            vrijeme = int(devs_project[1][0:len(devs_project)-1])
            
            brljudi = len(project1[devs_project[0]])
            total_profit += profit/brljudi
            total_hours += vrijeme/brljudi
        hourly_profit = total_profit / total_hours
        hourly_profit = int(hourly_profit)
        poruka+= f"Developer {developer} earns {hourly_profit} per hour\n"
    print(poruka)
proba()
        