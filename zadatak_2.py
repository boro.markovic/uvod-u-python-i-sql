from datetime import date

import json
import csv
#ucitavanje json fajla
json_file=open('covid-data.json')
covid_podaci=json.load(json_file)
#funkcija za dobijanje mjeseca i godine
def godina_mjesec(god,mjs):
    podaci=[]
    for date_info in covid_podaci:
        date=str(date_info['date'])
        godina=date[:4]
        mjesec=date[4:6]
        if god==godina and mjs==mjesec:
            podaci.append(date_info)
    return podaci

februar_covid=godina_mjesec('2021','02')

#csv fajl
covid_csv=open('covid_februar.csv','w',newline="")
csv_writer= csv.writer(covid_csv)
csv_writer.writerow(("Hospitalized","Positive"))

for februar in februar_covid:
    novired=(februar['positiveIncrease'],februar['hospitalizedIncrease'])
    csv_writer.writerow(novired)

#covid_csv.close()
#Funkcija za procenat zarazenih i hospitolizovanih

def hospital(date):
    date1=int(date.replace('-',''))
    for dan_info in covid_podaci:
        if dan_info["date"]==date1:
            br= dan_info["hospitalized"]/dan_info["positive"]*100
            br1=round(br,3)
            print(f"procenat hospitolizovanih u odnosu na zarane  je {br1}%")
   
hospital('2021-02-07')










