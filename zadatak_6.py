from abc import ABC,abstractstaticmethod
import re

class Film:
    def __init__(self,ime):
         self.ime=ime
    
    def ocijeni_film(self):
        pass

class SciFi(Film):
    def __init__(self, ime,efekti,vratolomije):
        super().__init__(ime)
        self.efekti=efekti
        self.vratolomije=vratolomije
    
    def ocijeni_film(self):
        return (self.efekti+self.vratolomije)/2

class Dokumentarac(Film):
    def __init__(self, ime,vjerodostojnost,gledanost):
        super().__init__(ime)
        self.vjerodostojnost=vjerodostojnost
        self.gledanost=gledanost
    
    def ocijeni_film(self):
        return (self.vjerodostojnost*self.gledanost/100)


x=[Film("The B"),
Film("The C and"),
Film("CCCC"),
Film("The Big Lebowski")]

def ime_film(x):
    return list(filter(lambda film: re.search ("The",film.ime) and not re.search("and",film.ime),x))

for film in ime_film(x):
    print(film.ime)


    