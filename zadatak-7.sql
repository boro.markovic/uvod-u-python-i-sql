
Create table proba.firmaodjeljenja(
idod numeric primary key,
naziv varchar(50),
lokacija varchar(50) default 'Podgorica' check(lokacija in ('Podgorica','Niksic','Bar','Zabljak')),
rukovodilac varchar(50));

create table proba.firmazaposleni(
id numeric(3) primary key check(id>=0 and id<=99),
od numeric,
ime varchar(40),
telefon varchar(12),
datum date,
foreign key(od) references firmaodjeljenja(idod)
);

insert into proba.firmaodjeljenja(idod,naziv,lokacija,rukovodilac) values(10,'Marketing','Podgorica','Petar Petrovic'),
(20,'HR','Niksic','Janko Jankovic'),(30,'Direktor','Zabljak','Marko Markovic'),(40,'IT','Bar','Ivan Ivanovic');
#unosenje rukovodilaca
select * from firmaodjeljenja;
insert into firmazaposleni(id,od,ime,telefon,datum) values(1,10,'Petar Petrovic','1234555','2018-01-01'),
(2,20,'Janko Jankovic','069777222','2019-01-02'),(3,30,'Marko Markovic','11112222','2015-06-21'),(4,40,'Ivan Ivanovic','092211222','2019-06-06');
insert into firmazaposleni(id,od,ime,telefon,datum) values(5,10,'Osoba1','3331111','2020-09-01'),(6,20,'Osoba2','222111','2019-12-01'),(7,40,'Osoba3','08911222','2018-11-11');
insert into firmazaposleni(id,od,ime,telefon,datum) values(8,20,'Marko Markovic','068777999','2020-10-10');
#unosenje zaposlenih
select ime from firmazaposleni; #selektujemo ime iz tabele firma zaposleni
select telefon from firmazaposleni x,firmaodjeljenja y where x.od=y.idod and y.lokacija='Niksic'; #selektujemo telefon zaposlenih iz Niksica
select min(y.datum) q,y.ime from proba.firmaodjeljenja x  left join proba.firmazaposleni y  on y.od=x.idod where ime='Marko Markovic' ; #Selekt za najstarijeg zaposlenog sa imenom marko markovic
select ime,od from firmazaposleni x, firmaodjeljenja y where  x.od=y.idod and y.idod=10;
delete from firmazaposleni where od=10;
